#!/usr/bin/env node

const Color = require('ansi-colors')
const parseArgs = require('minimist')
const package = require('../package.json')
const FS = require('fs')
const Path = require('path')
const syncDirectories = require('../lib/syncDirectories')
const exec = require('../lib/exec')
const axios = require('axios')
const log = require('../lib/log')
const { getCommitMessage } = require('../lib/shared')

const providers = [require('../provider/bitbucket'), require('../provider/blank')]
const provider = providers.find((provider) => provider.identify())

console.log(Color.white.bgBlue.bold('HV Save2Repo'), Color.yellow.bgBlue.bold(`v${package.version}`))

// Parsing Arguments
// =================

const ARGS = Object.assign(
	{
		source: './build',
		destination: null,
		branch: null,
		path: null,
		git_email: 'hvdevice@gmail.com',
		git_name: 'HV Build',
		number: null,
		help: null,
		version: null,
		tag: false,
	},
	provider.getOptions(),
	parseArgs(process.argv, {
		alias: {
			b: 'branch',
			s: 'source',
			d: 'destination',
			p: 'path',
			h: 'help',
			v: 'version',
			t: 'tag',
		},
	})
)

let hasInputError = false

if (ARGS.version) {
	log(`save2repo version: ${package.version}`)
	process.exit()
}
if (ARGS.help) {
	log(`
USAGE:

save2repo --source ./build

-s | --source path/to/source

    Website directory to upload. Default: ./build

-p | --path bitbucket_user/bitbucket_repo

    The bitbucket repository path (will only be considered if destination is not set).
    Default: $BITBUCKET_REPO_OWNER/$BITBUCKET_REPO_SLUG-build

    So if your current repository is hinderlingvolkart/project
    your destination path will be hinderlingvolkart/project-build

-d | --destination https://user:password@domain.com/path/to/repository.git

    The destination repository url with credentials.
    Default: Based on current bitbucket repository, see path parameter.

-b | --branch master

    The destination branch

-t | --tag tag

    Add tag to a branch. Default is the current version number (e.g. v1.2.45)

-h | --help

    Prints this help.
`)
	hasInputError = true
}
if (!ARGS.source) {
	log('- Missing: source', 'red')
	hasInputError = true
}
if (!ARGS.branch) {
	log('- Missing: branch or $BITBUCKET_BRANCH', 'red')
	hasInputError = true
}
if (!ARGS.path && !ARGS.destination) {
	log('- Missing: destination or $BITBUCKET_REPO_OWNER/$BITBUCKET_REPO_SLUG', 'red')
	hasInputError = true
}
if (hasInputError) {
	console.dir(ARGS, { colors: true })
	process.exit()
}

let repoUrl = ARGS.destination
let sourceDirectory = ARGS.source
let branchName = ARGS.branch
let buildNumber = ARGS.number
let gitEmail = ARGS.git_email
let gitName = ARGS.git_name
let tag = ARGS.tag

const repoDir = '__repo__'

async function save2repo() {
	if (!repoUrl) {
		repoUrl = await provider.getRepository(ARGS)
	}

	const output = {
		branch: branchName,
		origin: repoUrl.replace(/(https?:\/\/)[^\/]+@/, '$1'), // remove authentication
	}

	let hvPublishOutput
	try {
		hvPublishOutput = JSON.parse(FS.readFileSync('.hv-publish.json'))
	} catch (error) { }

	log(`Destination Repository: ${repoUrl}`)

	const gitLog = await exec(`git log --pretty=format:"%s"`)

	log(`Checking if branch ${branchName} already exists ...`)
	const branchExists = await exec(`git ls-remote ${repoUrl} refs/*/${branchName}`)
	log(`→  Branch exists: ${branchExists}`)

	// create repository directory
	await exec(`mkdir ${repoDir};`)
	process.chdir(repoDir)

	await exec(`echo $\{PWD##*/}`)

	if (branchExists) {
		log(`- Branch ${branchName} already exists, checking out.`)
		await exec(`git clone --branch ${branchName} --depth 10 ${repoUrl} .`)
	} else {
		log(`- Branch name ${branchName} doesn't exist yet - will create.`)
		await exec(`git clone --depth 1 ${repoUrl} .`)
		await exec(`git checkout --orphan ${branchName}`)
		await exec(`git rm -rfq --ignore-unmatch .`)
	}

	await exec(`
		git config --global user.email ${gitEmail}
		git config --global user.name ${gitName}
		git config http.postBuffer 157286400
		touch .rsync-exclude.txt
		echo ".gitignore" >> .rsync-exclude.txt
		echo ".git" >> .rsync-exclude.txt
	`)

	const ignore = FS.readFileSync('.rsync-exclude.txt')
		.toString()
		.split(/[\r\n]+/)
		.filter(Boolean)
	await syncDirectories(Path.join('..', sourceDirectory), '.', { ignore })
	FS.writeFileSync('.gitlog', gitLog)

	await exec(`
		rm -f .rsync-exclude.txt
		git add -u
		git add -A .
	`)

	// find new commit messages
	{
		const gitdiff = await exec(`git diff --color=never --staged .gitlog`)
		const added = []
		const removed = []
		gitdiff.split(/[\n\r]+/).forEach((line) => {
			const message = line.substring(1).trim()
			if (line.startsWith('+') && !line.startsWith('++')) {
				return added.push(message)
			}
			if (line.startsWith('-') && !line.startsWith('--')) {
				return removed.push(message)
			}
		})
		/*
		we don't want to include added lines that were removed
		because such lines are bad diff (just moved line basically).
		The diff on bitbucket would sometimes look like this:

			index 0434c27..55c6852 100644
			--- a/.gitlog
			+++ b/.gitlog
			@@ -1,3 +1,4 @@
			+feat. improve icongroup, refs. ASAL-568
			feat: remove text5 from body1, refs. ASAL-550
			feat: improve jobblock replace, refs. ASAL-557
			feat: update page generation, refs. ASAL-566
			@@ -47,4 +48,3 @@ feat: add job data job, refs. ASAL-540
			Brings back weather
			Work around missing pdf download in download list(happens with draft data aka preview)
			-ASAL-526 Fix press release link header alignment
			-ASAL-526 Add shorter press release link list(as type variant)
			green
			+ASAL-526 Fix press release link header alignment
		
		Which of course is slightly stupid by the diff algorithm. But who are we to judge?
		*/
		output.messages = added.filter((message) => !removed.includes(message))
	}

	const gitDiff = await (async () => {
		try {
			return await exec(`git diff-index HEAD --`)
		} catch (error) {
			return true
		}
	})()

	log(`Git diff: ${gitDiff}`)

	if (gitDiff) {
		const message = getCommitMessage({
			...output,
			...hvPublishOutput,
		})
		await exec(`git commit -a -m "${message}"`)
		output.commit = (await exec(`git rev-parse HEAD`)).trim()
		await exec(`git push origin ${branchName}`)
		log(`✅  Pushed changes to build repository`)

		if (tag) {
			if (typeof tag === 'boolean' && hvPublishOutput && hvPublishOutput.deploy && hvPublishOutput.deploy.index) {
				tag = `v${hvPublishOutput.deploy.index}`
			}

			if (typeof tag === 'string') {
				await exec(`git tag ${tag}`)
				await exec(`git push origin ${tag}`)
				log(`✅  Pushed tag ${tag} to build repository`)
			} else {
				output.tag = tag
			}
		}
	} else {
		log(`🆗  No changes to previous build. Nothing to commit.`)
	}

	process.chdir('../')

	if (hvPublishOutput) {
		log('💉  Patching dept.dev', 'magenta')
		const { messages, ...build_repo } = output
		const data = {
			id: hvPublishOutput.key,
			build_repo,
		}
		const result = (
			await axios({
				url: `https://dept.dev/api/deploy?token=${env('HVIFY_TOKEN')}`,
				data,
				method: 'PATCH',
				responseType: 'json',
			})
		).data
		console.log('- sent: ')
		console.dir(data, { colors: true })
		console.log('- receive: ')
		console.dir(result, { colors: true })
		await exec(`rm -rf ${repoDir}`)
	} else {
		console.log('Saving .save2repo.json for potential hv-publish followup')
		FS.writeFileSync('.save2repo.json', JSON.stringify(output))
	}
	FS.writeFileSync('.save2repo.json', JSON.stringify(output))
	// remove repository directory
}

save2repo().then(() => log('✅  save2repo!'))

function env(key) {
	return process.env[key]
}
