# Changelog

## [2.0.12] - 2025-02-24

- Replaced `hv.dev` with `dept.dev` (endpoint)
